import util, math, random
from util import ValueIteration
from msgame import MSGame
import time
import os
import numpy as np
import collections
import sys

HEIGHT = int(sys.argv[1])
WIDTH = int(sys.argv[2])
NUM_MINES = int(sys.argv[3])
NUM_TRIALS = 10000000
INF = 10000
EPSILON = 0.3
DISCOUNT  = 1
WIN_REWARD = 30
LOSE_REWARD = -30
MOVE_REWARD = 1

#board[height][width]
# 11 - unopened

# minesweeper MDP
class MinesweeperMDP(util.MDP):
    def __init__(self):

        self.game = MSGame(HEIGHT, WIDTH, NUM_MINES)
        self.height = self.game.board_height
        self.width = self.game.board_width
        
        

    def reset_game(self):
        self.game = MSGame(HEIGHT, WIDTH, NUM_MINES)
        self.height = self.game.board_height
        self.width = self.game.board_width
        return self.game

    def startState(self):
        return self.game.return_board()

    def getState(self):
        return self.game.return_board()

    def actions(self, state):
        board = self.game.return_board()
        if board[0][0] == 11:
            return [(0,0)]
        action_list = []
        for i in range(self.height):
            for j in range(self.width):
                if board[i][j] == 11:
                    action_list.append((i,j))

        return action_list

    def discount(self):
        return DISCOUNT
    def get_reward(self, action):
        self.game.play_move('click', action[0], action[1])
        if self.game.game_status == 0:
            return LOSE_REWARD
        if self.game.game_status == 1:
            return WIN_REWARD
        return MOVE_REWARD
        





# Q learning

# Performs Q-learning.  Read util.RLAlgorithm for more information.
# actions: a function that takes a state and returns a list of actions.
# discount: a number between 0 and 1, which determines the discount factor
# featureExtractor: a function that takes a state and action and returns a list of (feature name, feature value) pairs.
# explorationProb: the epsilon value indicating how frequently the policy
# returns a random action
class QLearningAlgorithm(util.RLAlgorithm):
    def __init__(self, actions, discount, featureExtractor, explorationProb=0.2):
        self.actions = actions
        self.discount = discount
        self.featureExtractor = featureExtractor
        self.explorationProb = explorationProb
        self.weights = collections.defaultdict(float)
        self.numIters = 0

    # Return the Q function associated with the weights and features
    def getQ(self, state, action):
        score = 0
        for f, v in self.featureExtractor(state, action):
            score += self.weights[f] * v
        return score

    # This algorithm will produce an action given a state.
    # Here we use the epsilon-greedy algorithm: with probability
    # |explorationProb|, take a random action.
    def getAction(self, state):
        self.numIters += 1
        if random.random() < self.explorationProb:
            return random.choice(self.actions(state))
        else:
            return max((self.getQ(state, action), action) for action in self.actions(state))[1]

    # Call this function to get the step size to update the weights.
    def getStepSize(self):
        return 1.0 / self.numIters

    # We will call this function with (s, a, r, s'), which you should use to update |weights|.
    # Note that if s is a terminal state, then s' will be None. 
    # You should update the weights using self.getStepSize(); use
    # self.getQ() to compute the current estimate of the parameters.
    def incorporateFeedback(self, state, action, reward, newState):
        def incrementSparseVector(v1, scale, v2):
            for item in v2.keys():
                if not v1[item]:
                    v1[item] = 0.0
                v1[item] += scale*v2[item]
            
        def get_V(state):
            a = []
            for action in self.actions(state):
                a.append(self.getQ(state, action))
            return max(a)

        def feature_map(state, action):
            
            m = collections.defaultdict(float)
            for f, v in self.featureExtractor(state, action):
                m[f] = v
            return m

        
        if newState == None:
            return
        incrementSparseVector(self.weights,-1*self.getStepSize()*(self.getQ(state,action) - (reward + self.discount*get_V(newState))), feature_map(state,action))


# Feature extractor: number of 1s 2s etc and sum
def get_neighbors_feature(state, x, y):
    m = collections.defaultdict(int, {})
    s = []
    s2 = 0.0
    for i in range(max(0, x-1), min(x+2,HEIGHT)):
        for j in range(max(0,y-1), min(y+2, WIDTH)):
            if not (i == x and  j ==y) :
                m[state[i][j]] += 1
                if state[x][y] >0 and state[x][y] <= 8:
                    s.append(state[x][y])
                    s2 += state[x][y]
    
    m['sum'] = s2
    return m

# Feature extractor: number of 1s 2s etc 
def get_neighbors_feature2(state, x, y):
    m = collections.defaultdict(int, {})
    for i in range(max(0, x-1), min(x+2,HEIGHT)):
        for j in range(max(0,y-1), min(y+2, WIDTH)):
            if not (i == x and  j ==y) :
                m[state[i][j]] += 1
    return m

# Feature extractor: number of 1s 2s etc in a neighborhood of 2 tiles
def get_neighbors_feature3(state,x,y):
    m = collections.defaultdict(int,{})
    for i in range(max(0, x-2), min(x+3,HEIGHT)):
        for j in range(max(0,y-2), min(y+3, WIDTH)):
            if not (i == x and  j ==y) :
                m[((i-x,j-y),state[i][j])] = 1      
    return m

# Return a singleton list containing indicator feature for the (state, action)
# pair.  Provides no generalization.
def FeatureExtractor(state, action):
    x = action[0]
    y = action[1]
    return get_neighbors_feature3(state, x, y).items()
    


############################################################

# Perform |numTrials| of the following:
# On each trial, take the MDP |mdp| and an RLAlgorithm |rl| and simulates the
# RL algorithm according to the dynamics of the MDP.
# Each trial will run for at most |maxIterations|.
# Return the list of rewards that we get for each trial.
def train():
    # play the game for ITER trials after learning by setting exploration probability to 0.
    def play(rl, MDP, ITER):
        wins = 0
        num_moves = 0
        open_frac = 0
        for _ in range(ITER):
            game = MDP.reset_game()
            state = MDP.startState()
            while game.game_status == 2: # while game is not over
                action = rl.getAction(state)
                MDP.get_reward(action)
                state = MDP.getState()
                num_moves += 1
            if game.game_status == 1:
                wins += 1
            board = game.return_board()
            ct = 0
            for i in range(HEIGHT):
                for j in range(WIDTH):
                    if board[i][j] == 11:
                        ct += 1
            open_frac += (WIDTH*HEIGHT-ct)*1.0/(WIDTH*HEIGHT - NUM_MINES)
        print "###############################################"
        print "win % ", wins*1.0/ITER," avg num moves ", num_moves*1.0/ITER, " avg open frac ",open_frac*1.0/ITER
        print "###############################################"
        time.sleep(5)


     
    MDP = MinesweeperMDP()
    game = MDP.game
    rl = QLearningAlgorithm(MDP.actions, MDP.discount(), FeatureExtractor, EPSILON)
    # train for NUM_TRIAL games
    for i in range(NUM_TRIALS):
        state = MDP.startState()
        totalDiscount = 1
        totalReward = 0
        num_moves = 0
        # if i% 10000 == 0:
        #     os.system("clear")
        #     print game.print_board()
        #     time.sleep(1)
        while game.game_status == 2: # while game is not over
            action = rl.getAction(state)
            reward = MDP.get_reward(action)
            newstate = MDP.getState()
            rl.incorporateFeedback(state,action,reward, newstate)
            totalReward += totalDiscount * reward
            totalDiscount *= MDP.discount()
            state = newstate
            num_moves += 1
            # if i% 10000 == 0:
            #     os.system("clear")
            #     print game.print_board()
            #     time.sleep(1)
        rl.incorporateFeedback(state, action, 0, None)
        print "Game ",i," : Total Reward = ",totalReward, " status ", game.game_status, " Num moves ", num_moves, 
        print rl.weights
        # calculate fraction opened
        board = game.return_board()
        ct = 0
        for k in range(HEIGHT):
            for j in range(WIDTH):
                if board[k][j] == 11:
                    ct += 1
        print "fraction opened ",(WIDTH*HEIGHT-ct)*1.0/(WIDTH*HEIGHT - NUM_MINES)
        game = MDP.reset_game()

    play(rl, MDP, 100)
    print rl.weights

train()
