from msgame import MSGame
from random import *
import time
import os
import numpy as np
from constraint import *
import collections
import sys

HEIGHT = int(sys.argv[1])
WIDTH = int(sys.argv[2])
NUM_MINES = int(sys.argv[3])
NUM_TRIALS = 100
INF = 10000
epsilon = 1
def play_game(game, alg):
	while game.game_status ==2:
		move,i,j = alg(game.return_board(), WIDTH, HEIGHT)
		game.play_move(move,i,j)
		# os.system('clear')
		#game.print_board()
		#time.sleep(0.2)
	## compute % of board opened
	ct = 0
	board = game.return_board()
	for i in range(HEIGHT):
		for j in range(WIDTH):
			if board[i][j] == 11:
				ct += 1
	# if game.game_status== 0:
	# 	time.sleep(3)
	return (game.game_status, game.num_moves, (WIDTH*HEIGHT-ct)*1.0/(WIDTH*HEIGHT - NUM_MINES))
# returns true if its a uponened box on the boundary
def boundary_unopened_box ( b , x, y):
	if b[x][y] != 11:
		return False
	for i in range(max(0, x-1), min(x+2,HEIGHT)):
		for j in range(max(0,y-1), min(y+2, WIDTH)):
			if not (i == x and  j ==y) :
				if b[i][j] >= 0 and b[i][j] <=8:
					return True
 # returns true if its a opened box on the boundary
def boundary_opened_box(b, x, y):
	if not(b[x][y] >=0 and b[x][y] <= 8):
		return False
	for i in range(max(0, x-1), min(x+2,HEIGHT)):
		for j in range(max(0,y-1), min(y+2, WIDTH)):
			if not (i == x and  j ==y) :
				if b[i][j] == 11:
					return True
# returns list of variable names of all unopened boxes in the neighborhood of the opened bos (i,j)
def unopened_neighbor_variables(b, x, y, tile_to_variable):
	l = []
	for i in range(max(0, x-1), min(x+2,HEIGHT)):
		for j in range(max(0,y-1), min(y+2, WIDTH)):
			if not (i == x and  j ==y) :
				if b[i][j] == 11:
					l.append(tile_to_variable[(i,j)])
	return l


def csp_player(board, height, width):
	if board[0][0] == 11:
		return 'click', 0, 0
	problem = Problem()
	var_index = 0
	variable_to_tile = {}
	tile_to_variable = {}
	for i in range(height):
		for j in range(width):
			if boundary_unopened_box(board, i, j):
				variable_to_tile[var_index] = (i,j)
				tile_to_variable[(i,j)] = var_index
				problem.addVariable(var_index, [0,1])
				var_index += 1
	for i in range(height):
		for j in range(width):
			if boundary_opened_box(board, i, j):
				problem.addConstraint(ExactSumConstraint(board[i][j]), unopened_neighbor_variables(board, i, j, tile_to_variable))
	#print "start"
	solutions = problem.getSolutions()
	#print "end"
	#print solutions, variable_to_tile

	count = collections.defaultdict(int,{})

	for i in range(var_index):
		count[i] = 0
		for m in solutions:
			if m[i] == 1:
				count[i] += 1
	l = []
	for var in count:
		l.append((count[var], var))
	l.sort()
	#print l
	tile_list = get_covered_tile_list(board,height, width, tile_to_variable.keys())
	if l[0][0] >= epsilon * len(solutions) and tile_list!=[]:

		print "Random Move"
		temp = random_tile(board, height, width, tile_list)
		#print temp
		return temp
	temp =  'click', variable_to_tile[l[0][1]][0], variable_to_tile[l[0][1]][1]
	#print temp
	return temp

def get_covered_tile_list(board, height, width, fringe):
	l = []
	for i in range(height):
		for j in range(width):
			if board[i][j] == 11 and ((i,j) not in fringe):
				l.append((i,j))
	return l



def random_tile(board, height, width, tile_list):
	if board[0][0] == 11:
		return 'click', 0, 0

	ret = choice(tile_list)
	return 'click', ret[0], ret[1]


####################### Run CSP player
csp_results = []
ct = 0
while ct < NUM_TRIALS:
	game = MSGame(HEIGHT, WIDTH, NUM_MINES)
	temp = play_game(game, csp_player)
	if temp[1] > 2:
		csp_results.append(temp)
		print temp
		ct += 1

#print csp_results
wins = [x[0] for x in csp_results]
num_moves = [x[1] for x in csp_results]
opened_board = [x[2] for x in csp_results]
print "% of wins by CSP selection = ", sum(wins)*1.0/NUM_TRIALS
print "avg number of moves survived by CSP selection = ", sum(num_moves)*1.0/NUM_TRIALS
print "avg % of board opened by CSP selection = ", sum(opened_board)*100.0/NUM_TRIALS
