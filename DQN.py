#KERAS_BACKEND=theano python DQN.py
#KERAS_BACKEND=theano python -c "from keras import backend"
import util, math, random
from util import ValueIteration
from msgame import MSGame
import time
import os
import numpy as np
import collections
from keras.models import Sequential
from keras.layers import *
from keras.optimizers import *
import sys
HEIGHT = int(sys.argv[1])
WIDTH = int(sys.argv[2])
NUM_MINES = int(sys.argv[3])
TESTGAMES = 1000
NUM_TRIALS = 30000000

INF = 10000
EPSILON = 0.3
DISCOUNT  = 1
WIN_REWARD = 30
LOSE_REWARD = -30
MOVE_REWARD = 1
FRINGE_REWARD = 2
boundary = 2
DIMENSION = (2*boundary+1)*(2*boundary+1)*10


#board[height][width]
# 11 - unopened

# minesweeper MDP
class MinesweeperMDP(util.MDP):
    def __init__(self):

        self.game = MSGame(HEIGHT, WIDTH, NUM_MINES)
        self.height = self.game.board_height
        self.width = self.game.board_width



    def reset_game(self):
        self.game = MSGame(HEIGHT, WIDTH, NUM_MINES)
        self.height = self.game.board_height
        self.width = self.game.board_width
        return self.game

    def startState(self):
        return self.game.return_board()

    def getState(self):
        return self.game.return_board()
    def boundary_unopened_box (self, b , x, y):
        if b[x][y] != 11:
            return False
        for i in range(max(0, x-1), min(x+2,HEIGHT)):
            for j in range(max(0,y-1), min(y+2, WIDTH)):
                if not (i == x and  j ==y) :
                    if b[i][j] >= 0 and b[i][j] <=8:
                        return True
        return False

    def actions(self, state):
        board = self.game.return_board()
        if board[0][0] == 11:
            return [(0,0)]
        action_list = []
        for i in range(self.height):
            for j in range(self.width):
                if board[i][j] == 11:
                    action_list.append((i,j))

        return action_list

    def discount(self):
        return DISCOUNT
    def get_reward(self, action):
        reward = 0
        if self.boundary_unopened_box(self.game.return_board(), action[0], action[1]):
            reward += FRINGE_REWARD
        self.game.play_move('click', action[0], action[1])
        if self.game.game_status == 0:
            return LOSE_REWARD
        if self.game.game_status == 1:
            return WIN_REWARD
        return MOVE_REWARD + reward






#Q learning

# Performs Q-learning.  Read util.RLAlgorithm for more information.
# actions: a function that takes a state and returns a list of actions.
# discount: a number between 0 and 1, which determines the discount factor
# featureExtractor: a function that takes a state and action and returns a list of (feature name, feature value) pairs.
# explorationProb: the epsilon value indicating how frequently the policy
# returns a random action
class DQLearningAlgorithm(util.RLAlgorithm):
    def __init__(self, actions, discount, featureExtractor, dimension, explorationProb=0.2):
        self.actions = actions
        self.discount = discount
        self.featureExtractor = featureExtractor
        self.explorationProb = explorationProb
        #self.weights = collections.defaultdict(float)
        self.numIters = 0
        self.dim = dimension
        self.model = self._createModel()

    #create a Nueral Network model for function approximator
    def _createModel(self):
        model = Sequential()

        model.add(Dense(output_dim=64, activation='relu', input_dim=self.dim))
        model.add(Dense(output_dim=1, activation='linear'))

        opt = RMSprop(lr=0.00025)
        model.compile(loss='mse', optimizer=opt)

        return model

    #train the NN with this data point
    def train(self, x, y, epoch=1, verbose=0):
        #print 'x is: ', x
        #print 'y is: ', y
        self.model.fit(np.array([x]), np.array([y]), batch_size=1, nb_epoch=epoch, verbose=verbose)
    # def train(self, x, y):
    #     self.model.fit(x.reshape(1, self.dim), y)

    #predict for this list of data points
    def predict(self, s):
        return self.model.predict(s)

    #predict for one data points
    def predictOne(self, s):
        #s = np.array(s)
        #print s.shape, self.dim
        return self.predict(s.reshape(1, self.dim)).flatten()

    # Return the Q function associated with the weights and features
    def getQ(self, state, action):
        return self.predictOne(self.featureExtractor(state, action))[0]


    # This algorithm will produce an action given a state.
    # Here we use the epsilon-greedy algorithm: with probability
    # |explorationProb|, take a random action.
    def getAction(self, state):
        self.numIters += 1
        if random.random() < self.explorationProb:
            return random.choice(self.actions(state))
        else:
            return max((self.getQ(state, action), action) for action in self.actions(state))[1]

    # Call this function to get the step size to update the weights.
    def getStepSize(self):
        return 1.0 / self.numIters

    # We will call this function with (s, a, r, s'), which you should use to update |weights|.
    # Note that if s is a terminal state, then s' will be None.  Remember to check for this.
    # You should update the weights using self.getStepSize(); use
    # self.getQ() to compute the current estimate of the parameters.
    def incorporateFeedback(self, state, action, reward, newState):
        def get_V(state):
            a = []
            for action in self.actions(state):
                a.append(self.getQ(state, action))
            return max(a)

        if newState == None:
            return

        self.train(self.featureExtractor(state, action), reward + self.discount*get_V(newState) )



def get_neighbors_feature(state, x, y):
    m = collections.defaultdict(int, {})
    for i in range(-1*boundary, boundary + 1):
        for j in range(-1*boundary, boundary + 1):
            m[(i,j)] = 11


    for i in range(max(0, x-boundary), min(x+boundary+1,HEIGHT)):
        for j in range(max(0,y-boundary), min(y+boundary+1, WIDTH)):
            if not (i == x and  j ==y) :
                m[(i-x, j-y)] = state[i][j]
    l = sorted(m.items())
    return [x[1] for x in l]


def get_neighbors_feature_array(state, x, y):
    m = collections.defaultdict(int, {})
    for i in range(-1*boundary, boundary + 1):
        for j in range(-1*boundary, boundary + 1):
            for k in range(0,10):
                m[(i,j,k)] = 0

    for i in range(max(0, x-boundary), min(x+boundary+1,HEIGHT)):
        for j in range(max(0,y-boundary), min(y+boundary+1, WIDTH)):
            if not (i == x and  j ==y) :
                if state[i][j] == 11:
                    m[(i-x, j-y, 9)] = 1
                elif state[i][j] in [0,1,2,3,4,5,6,7,8]:
                    m[(i-x, j-y, state[i][j])] = 1
    #print m
    l = sorted(m.items())
    return [x[1] for x in l]




# Return a singleton list containing indicator feature for the (state, action)
# pair.  Provides no generalization.
def FeatureExtractor(state, action):
    x = action[0]
    y = action[1]

    return np.array(get_neighbors_feature_array(state, x, y))



############################################################

# Perform |numTrials| of the following:
# On each trial, take the MDP |mdp| and an RLAlgorithm |rl| and simulates the
# RL algorithm according to the dynamics of the MDP.
# Each trial will run for at most |maxIterations|.
# Return the list of rewards that we get for each trial.
def train():
    def play(rl, MDP, ITER):
        wins = 0
        num_moves = 0
        open_frac = 0
        rl.explorationProb = 0
        for it in range(ITER):
            game = MDP.reset_game()
            state = MDP.startState()
            while game.game_status == 2: # while game is not over
                if it % 100 == 0:
                    print game.print_board()
                    time.sleep(1)
                action = rl.getAction(state)
                MDP.get_reward(action)
                state = MDP.getState()
                num_moves += 1
            if game.game_status == 1:
                wins += 1
            board = game.return_board()
            ct = 0
            for i in range(HEIGHT):
                for j in range(WIDTH):
                    if board[i][j] == 11:
                        ct += 1
            open_frac += (WIDTH*HEIGHT-ct)*1.0/(WIDTH*HEIGHT - NUM_MINES)
        print "###############################################"
        print "win % ", wins*1.0/ITER," avg num moves ", num_moves*1.0/ITER, " avg open frac ",open_frac*1.0/ITER
        print "###############################################"
        time.sleep(5)



    MDP = MinesweeperMDP()
    game = MDP.game
    rl = DQLearningAlgorithm(MDP.actions, MDP.discount(), FeatureExtractor, DIMENSION, EPSILON)
    # train for NUM_TRIAL games
    for i in range(NUM_TRIALS):
        state = MDP.startState()
        totalDiscount = 1
        totalReward = 0
        num_moves = 0
        # if i% 10000 == 0:
        #     os.system("clear")
        #     print game.print_board()
        #     time.sleep(1)
        while game.game_status == 2: # while game is not over
            action = rl.getAction(state)
            reward = MDP.get_reward(action)
            newstate = MDP.getState()
            rl.incorporateFeedback(state,action,reward, newstate)
            totalReward += totalDiscount * reward
            totalDiscount *= MDP.discount()
            state = newstate
            num_moves += 1
            if i% 10000 == 0:
                os.system("clear")
                print game.print_board()
                time.sleep(1)
        rl.incorporateFeedback(state, action, 0, None)

        print "Game ",i," : Total Reward = ",totalReward, " status ", game.game_status, " Num moves ", num_moves, 

        #print "Game ",i," : Total Reward = ",totalReward, " status ", game.game_status, " Num moves ", num_moves,
        if i%10000:
            print i

        # calculate fraction opened
        board = game.return_board()
        ct = 0
        for k in range(HEIGHT):
            for j in range(WIDTH):
                if board[k][j] == 11:
                    ct += 1
        #print "fraction opened ",(WIDTH*HEIGHT-ct)*1.0/(WIDTH*HEIGHT - NUM_MINES)
        game = MDP.reset_game()

    play(rl, MDP, TESTGAMES)



print "Hello"
train()
