
from msgame import MSGame
from random import *
import time
import os
import numpy as np
import sys
HEIGHT = int(sys.argv[1])
WIDTH = int(sys.argv[2])
NUM_MINES = int(sys.argv[3])
NUM_TRIALS = 100
INF = 10000
epsilon = 0.2

#fubtion to play the game with the given algorithm 'alg'. It calls alg to get the next move
def play_game(game, alg):
	while game.game_status ==2:
		move,i,j = alg(game.return_board(), WIDTH, HEIGHT)
		game.play_move(move,i,j)
		# os.system('clear')
		# game.print_board()
		# time.sleep(1)
	## compute % of board opened
	ct = 0
	board = game.return_board()
	for i in range(HEIGHT):
		for j in range(WIDTH):
			if board[i][j] == 11:
				ct += 1

	return (game.game_status, game.num_moves, (WIDTH*HEIGHT-ct)*1.0/(WIDTH*HEIGHT - NUM_MINES))

# random baseline algorithm
def random_baseline(board, height, width):
	if board[0][0] == 11:
		return 'click', 0, 0
	flag = True

	while flag:
		i = randint(0,height - 1)
		j = randint(0,width - 1)
		if board[i][j] == 11:
			move = 'click'
			y = i
			x = j
			flag = False
	return move, y, x

def func_max ( b , i, j):
	s = []
	for x in [i-1,i,i+1]:
		for y in [j-1, j, j+1]:
			if b[x][y] >0 and b[x][y] <= 8:
				s.append(b[x][y])
	if s == []:
		return INF
	return max(s)

def func_sum ( b , i, j):
	s = 0
	for x in [i-1,i,i+1]:
		for y in [j-1, j, j+1]:
			if b[x][y] >0 and b[x][y] <= 8:
				s += b[x][y]
	if s == 0:
		return INF
	return s

#Given a 'func' returns a algorithm that uses func
def get_baseline(func):
	def other_baseline(board,height, width):
		if board[0][0] == 11:
			return 'click', 0, 0
		new_board = np.zeros((height + 2,width + 2))
		for i in range(height):
			for j in range(width):
				new_board[i+1][j+1] = board[i][j]
		m = (INF,0,0)
		for i in range(1,height + 2):
			for j in range(1, width + 2):
				if new_board[i][j] == 11:
					val = func(new_board, i , j)
					if val < m[0]:
						m = (val,i-1,j-1)
		# if uniform(0,1) < epsilon:
		# 	return random_baseline(board, height, width)
		return 'click', m[1],m[2]

	return other_baseline



#### run random baseline and report results
random_results = []
for _ in range(NUM_TRIALS):
	game = MSGame(HEIGHT, WIDTH, NUM_MINES)
	random_results.append(play_game(game, random_baseline))
#print random_results
wins = [x[0] for x in random_results] 
num_moves = [x[1] for x in random_results]
opened_board = [x[2] for x in random_results]
print "% of wins by random selection = ", sum(wins)*1.0/NUM_TRIALS
print "avg number of moves survived by random selection = ", sum(num_moves)*1.0/NUM_TRIALS
print "avg % of board opened by random selection = ", sum(opened_board)*100.0/NUM_TRIALS


############# Baseine with sum of neighborhood
results = []
for _ in range(NUM_TRIALS):
	game = MSGame(HEIGHT, WIDTH, NUM_MINES)
	results.append(play_game(game, get_baseline(func_sum)))
wins = [x[0] for x in results] 
num_moves = [x[1] for x in results]
opened_board = [x[2] for x in results]
print "% of wins by sum selection = ", sum(wins)*1.0/NUM_TRIALS
print "avg number of moves survived by sum selection = ", sum(num_moves)*1.0/NUM_TRIALS
print "avg % of board opened by sum selection = ", sum(opened_board)*100.0/NUM_TRIALS


############# Baseine with Max of neighborhood
results = []
for _ in range(NUM_TRIALS):
	game = MSGame(HEIGHT, WIDTH, NUM_MINES)
	results.append(play_game(game, get_baseline(func_max)))
wins = [x[0] for x in results] 
num_moves = [x[1] for x in results]
opened_board = [x[2] for x in results]
print "% of wins by max selection = ", sum(wins)*1.0/NUM_TRIALS
print "avg number of moves survived by max selection = ", sum(num_moves)*1.0/NUM_TRIALS
print "avg % of board opened by max selection = ", sum(opened_board)*100.0/NUM_TRIALS
